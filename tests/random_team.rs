use rand::prelude::*;
use std::io::Write;
use std::path::PathBuf;
use team_maker::*;

#[test]
fn random_teams() {
    let players: Vec<Player> = PlayerStatsHistory::from_path(&PathBuf::from("./data.csv"))
        .expect("Error while reading player stats in \"data.csv\"")
        .global
        .values()
        .cloned()
        .collect();
    let mut rng = thread_rng();

    let mut stdout = std::io::stdout();

    for _ in 0..10 {
        let n = rng.gen_range(3, players.len());

        let players: Vec<Player> = players.choose_multiple(&mut rng, n).cloned().collect();

        let teams = find_teams(&players, &[]);

        print_teams(&mut stdout, &teams).unwrap();

        writeln!(stdout, "\n -- \n").unwrap();
    }
}
