use super::Player;

const TEAM_LEN_MULTIPLIER: f32 = 20.0; // 10.0

#[derive(Debug)]
pub struct Team<'a> {
    score: f32,
    players: Vec<&'a Player>,
}

impl<'a> Team<'a> {
    pub fn empty() -> Self {
        Self {
            players: Vec::new(),
            score: 0.0,
        }
    }
    pub fn from_mask(mask: usize, players: &'a [Player]) -> (Self, Self) {
        let mut first_team = Vec::new();
        let mut second_team = Vec::new();

        for (i, player) in players.iter().enumerate() {
            if mask & (1 << i) != 0 {
                second_team.push(player);
            } else {
                first_team.push(player);
            }
        }

        first_team.sort_by_key(|p| (p.rating() * -100.) as isize);
        second_team.sort_by_key(|p| (p.rating() * -100.0) as isize);

        (Self::from(first_team), Self::from(second_team))
    }

    fn from(players: Vec<&'a Player>) -> Self {
        let score = players.iter().fold(0.0, |v, p| v + p.rws())
            + players.len() as f32 * TEAM_LEN_MULTIPLIER;
        Self { score, players }
    }

    pub fn score(&self) -> f32 {
        self.score
    }

    pub fn iter(&self) -> impl Iterator<Item = &Player> {
        self.players.iter().copied()
    }

    pub fn len(&self) -> usize {
        self.players.len()
    }

    pub fn avg_player_score(&self) -> f32 {
        self.players.iter().map(|p| p.rws()).sum::<f32>() / self.players.len() as f32
    }

    pub fn var_player_score(&self) -> f32 {
        let mu = self.avg_player_score();

        self.players
            .iter()
            .map(|p| (p.rws() - mu).powi(2))
            .sum::<f32>()
            / self.players.len() as f32
    }
}
