mod args;

use structopt::StructOpt;
use team_maker::{find_teams, print_teams};

fn main() {
    env_logger::builder().format_timestamp(None).init();

    let args = args::Args::from_args();

    let players_stats = args
        .load_player_stats()
        .expect("Error while deserialization of player data");

    let players_stats = if args.global {
        players_stats.global
    } else {
        players_stats.avg_stats_of_last(args.stats_window)
    };

    let player_list = args
        .load_players()
        .expect("Error while reading list of players");

    let mut players = Vec::with_capacity(player_list.len());

    for player in player_list {
        if let Some(player) = players_stats.get(&player) {
            players.push(player.clone());
        } else {
            // MISSING STATS FOR PLAYER
            log::error!("Unknown player {:?}", player);
        }
    }

    // Start finding teams
    let teams = find_teams(&players, &args.constraints(&players));
    print_teams(&mut std::io::stdout(), &teams).unwrap();
}
