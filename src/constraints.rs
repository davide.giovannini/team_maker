use crate::player::SteamId;
use crate::{Player, Team};
use std::path::Path;

// TODO manually impl Eq, Ord etc to make order of player1 player2 interchangeable
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Constraint {
    player1: SteamId,
    player2: SteamId,
    kind: ConstraintType,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
enum ConstraintType {
    SameTeam,
    DifferentTeam,
}

impl Constraint {
    pub fn is_valid(&self, players: &[Player]) -> bool {
        let player1_exists = players.iter().any(|p| p.steam_id() == self.player1);
        let player2_exists = players.iter().any(|p| p.steam_id() == self.player2);
        player1_exists && player2_exists
    }

    pub fn check(&self, team: &Team) -> bool {
        let player1_in_team = team.iter().any(|p| p.steam_id() == self.player1);
        let player2_in_team = team.iter().any(|p| p.steam_id() == self.player2);

        let same_team =
            (player1_in_team && player2_in_team) || (!player1_in_team && !player2_in_team);

        match self.kind {
            ConstraintType::SameTeam => same_team,
            ConstraintType::DifferentTeam => !same_team,
        }
    }

    pub fn load_from_file(path: &Path) -> std::io::Result<Vec<Constraint>> {
        Ok(std::fs::read_to_string(path)?
            .lines()
            .flat_map(Self::try_parse)
            .flatten()
            .collect())
    }

    pub fn try_parse(line: &str) -> Result<Option<Self>, String> {
        let line = line.trim();
        if line.starts_with('#') {
            return Ok(None);
        }

        let kind = if line.starts_with('!') {
            ConstraintType::DifferentTeam
        } else {
            ConstraintType::SameTeam
        };

        let line = line.trim_start_matches('!').trim();

        let pieces: Vec<&str> = line.trim().split_whitespace().collect();
        if pieces.len() == 2 {
            Ok(Some(Constraint {
                player1: pieces[0]
                    .parse()
                    .map_err(|e: std::num::ParseIntError| e.to_string())?,
                player2: pieces[1]
                    .parse()
                    .map_err(|e: std::num::ParseIntError| e.to_string())?,
                kind,
            }))
        } else {
            Err(format!("Invalid constraint: {:?})", line))
        }
    }
}

impl std::fmt::Display for Constraint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let prefix = if self.kind == ConstraintType::DifferentTeam {
            "!"
        } else {
            ""
        };
        write!(f, "{}{} {}", prefix, self.player1, self.player2)
    }
}

impl std::fmt::Debug for Constraint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} and {} on {} team",
            self.player1,
            self.player2,
            match self.kind {
                ConstraintType::SameTeam => "same",
                ConstraintType::DifferentTeam => "diff",
            }
        )
    }
}

#[cfg(test)]
mod test_constraint_parsing {
    use super::*;

    #[test]
    fn should_parse_diff_team() {
        let line = "!76561198445303300 76561198445303301";
        let expected = Some(Constraint {
            player1: 76561198445303300.into(),
            player2: 76561198445303301.into(),
            kind: ConstraintType::DifferentTeam,
        });

        assert_eq!(Constraint::try_parse(line), Ok(expected))
    }

    #[test]
    fn should_parse_same_team() {
        let line = "76561198445303300 76561198445303301";
        let expected = Some(Constraint {
            player1: 76561198445303300.into(),
            player2: 76561198445303301.into(),
            kind: ConstraintType::SameTeam,
        });

        assert_eq!(Constraint::try_parse(line), Ok(expected))
    }

    #[test]
    fn should_parse_comment() {
        let line = "#76561198445303300 76561198445303301";
        let expected = None;

        assert_eq!(Constraint::try_parse(line), Ok(expected))
    }

    #[test]
    fn should_reject() {
        let line = " sadasd 76561198445303300 76561198445303301";

        assert!(Constraint::try_parse(line).is_err())
    }

    #[test]
    fn should_parse_to_string_output() {
        let constraint1 = Constraint {
            player1: 76561198445303300.into(),
            player2: 76561198445303301.into(),
            kind: ConstraintType::DifferentTeam,
        };
        let constraint2 = Constraint {
            player1: 76561198445303300.into(),
            player2: 76561198445303301.into(),
            kind: ConstraintType::SameTeam,
        };

        assert_eq!(
            Constraint::try_parse(&constraint1.to_string()),
            Ok(Some(constraint1))
        );
        assert_eq!(
            Constraint::try_parse(&constraint2.to_string()),
            Ok(Some(constraint2))
        );
    }
}
