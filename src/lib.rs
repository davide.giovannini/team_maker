pub mod constraints;
mod player;
mod player_stats;
mod teams;
pub use crate::teams::Team;
pub use player::{Player, SteamId};
pub use player_stats::*;
use std::io::Write;

pub fn find_teams<'a, 'b>(
    player_list: &'a [Player],
    constraints: &'b [constraints::Constraint],
) -> (Team<'a>, Team<'a>) {
    let mut best_skill_gap = std::f32::INFINITY;
    let mut best_teams = (Team::empty(), Team::empty());
    let mut best_mask = 0;

    for mask in 1..(2_f32.powi(player_list.len() as i32) as usize - 1) {
        let (first, second) = Team::from_mask(mask, &player_list);

        // Skip unbalanced teams with more than 1 player difference
        if (first.len() as isize - second.len() as isize).abs() > 1 {
            continue;
        }

        if !constraints
            .iter()
            .all(|c| c.check(&first) && c.check(&second))
        {
            continue;
        }

        let skill_gap = (first.score() - second.score()).abs();

        if skill_gap < best_skill_gap {
            best_skill_gap = skill_gap;
            best_teams = (first, second);
            best_mask = mask;
        }
    }

    log::debug!("Mask: {}: {0:0>10b}", best_mask);

    best_teams
}

pub fn print_teams(w: &mut impl Write, teams: &(Team, Team)) -> std::io::Result<()> {
    print_team(w, &teams.0, "Team CT")?;
    print_team(w, &teams.1, "Team T")
}

fn print_team(w: &mut impl Write, team: &Team, name: &str) -> std::io::Result<()> {
    writeln!(w, "{:->30}", "")?;
    writeln!(w, "{:^30 }", name)?;
    writeln!(w, "{:->30}", "")?;

    for player in team.iter() {
        writeln!(w, "{:^30 }", player)?;
    }

    writeln!(w, "\nTotal score: {:.4}\n", team.score())?;
    Ok(())
}
