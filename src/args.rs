use std::io::BufRead;
use std::path::PathBuf;
use structopt::StructOpt;
use team_maker::constraints::Constraint;
use team_maker::{Player, PlayerStatsHistory, SteamId};

#[derive(Debug, StructOpt)]
pub struct Args {
    #[structopt(long, short)]
    pub stats_file: PathBuf,

    #[structopt(long, short)]
    pub players: PathBuf,

    #[structopt(long)]
    /// Players that need to be in the same team
    pub constraints: Option<PathBuf>,

    #[structopt(long)]
    /// Use global stats
    pub global: bool,

    #[structopt(long, default_value("8"))]
    /// Use avg stats of last N sessions
    pub stats_window: usize,
}

impl Args {
    pub fn load_player_stats(&self) -> std::io::Result<PlayerStatsHistory> {
        PlayerStatsHistory::from_path(&self.stats_file)
    }

    pub fn load_players(&self) -> std::io::Result<Vec<SteamId>> {
        let file = std::fs::File::open(&self.players)?;
        let reader = std::io::BufReader::new(file);

        let mut players = Vec::new();
        for line in reader.lines() {
            let line = line?.trim().to_string();
            if line.is_empty() || line.starts_with('#') {
                continue;
            }
            let player = line.parse::<SteamId>().expect("Could not parse SteamID");
            players.push(player);
        }

        Ok(players)
    }

    pub fn constraints(&self, players: &[Player]) -> Vec<Constraint> {
        let result = self
            .constraints
            .as_ref()
            .map(|p| Constraint::load_from_file(&p));

        match result {
            Some(Err(e)) => {
                log::error!("Error while loading constraints: {:#?}", e);
                Vec::new()
            }
            Some(Ok(mut vec)) => vec.drain(..).filter(|c| c.is_valid(players)).collect(),
            _ => Vec::new(),
        }
    }
}
