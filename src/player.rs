use serde::Deserialize;
use std::fmt::Formatter;
use std::str::FromStr;

#[derive(Clone, Copy, Deserialize, Default, Eq, PartialEq, Ord, PartialOrd, Debug)]
pub struct SteamId(usize);

impl std::fmt::Display for SteamId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl FromStr for SteamId {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let id: usize = s.parse()?;
        Ok(Self(id))
    }
}
impl From<usize> for SteamId {
    fn from(id: usize) -> Self {
        Self(id)
    }
}

#[derive(Clone, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Player {
    name: String,
    #[serde(alias = "SteamID")]
    steam_id: SteamId,
    rating: f32,
    #[serde(alias = "RWS")]
    rws: f32,
    #[serde(alias = "K/D")]
    kill_death_ratio: Option<f32>,
    #[serde(alias = "HS%")]
    headshots_ratio: Option<f32>,
}

impl Player {
    pub fn new(steam_id: SteamId, name: String, rating: f32, rws: f32) -> Self {
        Self {
            steam_id,
            name,
            rating,
            rws,
            kill_death_ratio: None,
            headshots_ratio: None,
        }
    }
    pub fn steam_id(&self) -> SteamId {
        self.steam_id
    }
    pub fn name(&self) -> String {
        self.name.clone()
    }

    pub fn rating(&self) -> f32 {
        self.rating
    }

    pub fn rws(&self) -> f32 {
        self.rws
    }
}

impl std::fmt::Display for Player {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        //format!("{}({}, rws {})", self.name, self.rating, self.rws).fmt(f)
        self.name.fmt(f)
    }
}

impl PartialEq for Player {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}
impl Eq for Player {}

impl std::fmt::Debug for Player {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}:\n\tsteam_id: {}\n\trating: {}\n\tRWS: {}",
            self.name, self.steam_id, self.rating, self.rws
        )?;
        if let Some(hs) = self.headshots_ratio {
            write!(f, "\n\tHS%: {}", hs)?;
        }
        if let Some(kd) = self.kill_death_ratio {
            write!(f, "\n\tK/D%: {}", kd)?;
        }
        Ok(())
    }
}
