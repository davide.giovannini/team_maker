use crate::{Player, SteamId};
use std::collections::BTreeMap;
use std::path::Path;

pub type PlayerStatistics = BTreeMap<SteamId, Player>;

#[derive(Debug)]
pub struct PlayerStatsHistory {
    pub global: PlayerStatistics,
    pub historical: Vec<(usize, PlayerStatistics)>,
}

impl PlayerStatsHistory {
    pub fn avg_stats_of_last(&self, n: usize) -> PlayerStatistics {
        let data = self.historical.iter().take(n);
        let mut aggregation: BTreeMap<SteamId, Vec<Player>> = BTreeMap::new();

        for (_, stats) in data {
            for (name, player) in stats {
                let entry = aggregation.entry(*name).or_default();
                entry.push(player.clone());
            }
        }

        aggregation
            .iter()
            .map(|(k, v)| (k.clone(), avg_player_stats(v)))
            .collect()
    }

    /// Load PlayerStatsHistory from csv
    pub fn from_path(path: &Path) -> std::io::Result<Self> {
        let data = std::fs::read_to_string(path)?;

        let re_stats_separator = regex::Regex::new(r"(?m)^,+\r?$").unwrap();
        let re_skip_line =
            regex::Regex::new(r"(?m)^(,\d+,\d+,+)|(,+Session #.+)|(\d+-\d+-\d+,\d+,\d+,+)\r?$")
                .unwrap();
        let re_skip_line_global_stats = regex::Regex::new(r"(?m)^(,\d+,\d+,+)\r?$").unwrap();
        let re_session_number = regex::Regex::new(r"(?m)^,+Session #:,(\d+).+\r?$").unwrap();

        let mut items = re_stats_separator.split(&data);

        let global_stats = items
            .next()
            .map(|text| re_skip_line_global_stats.replace_all(text, ""));

        let mut historical = Vec::new();
        for item in items {
            let session_number: usize = re_session_number
                .captures(item)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse()
                .unwrap();

            let stats = re_skip_line.replace_all(item, "");
            let session = load_player_stats(&stats).unwrap();
            if session.is_empty() {
                continue;
            }
            historical.push((session_number, session));
        }
        historical.sort_by_key(|&(n, _)| -(n as isize));

        Ok(Self {
            global: load_player_stats(&global_stats.unwrap()).unwrap(),
            historical,
        })
    }
}

fn load_player_stats(data: &str) -> csv::Result<PlayerStatistics> {
    let mut reader = csv::ReaderBuilder::new().from_reader(data.as_bytes());
    let mut players_data = BTreeMap::new();
    for player in reader.deserialize() {
        let player: Player = player?;
        players_data.insert(player.steam_id(), player);
    }
    Ok(players_data)
}

fn avg_player_stats(stats: &[Player]) -> Player {
    let mut name = Default::default();
    let mut steam_id = Default::default();
    let mut rating = 0.0;
    let mut rws = 0.0;

    for p in stats {
        name = p.name().clone();
        steam_id = p.steam_id();
        rating += p.rating();
        rws += p.rws();
    }

    let size = stats.len() as f32;

    Player::new(steam_id, name, rating / size, rws / size)
}
