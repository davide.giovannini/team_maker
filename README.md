# Compiling

```
cargo build --release
```

Will generate an exe in `./target/release/team_maker`



# Running

```
./team_maker -s data.csv -p players.txt
```

Without compiling first

```
cargo run -- -s data.csv -p players.txt
```


### Players.txt
List of available players.

One player per line,
use `#` to comment a line

```
iDg
#linnal
hadoop1
```


## Testing

To generate some random teams

```
cargo test -- --show-output
```

